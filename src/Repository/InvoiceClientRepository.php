<?php

namespace App\Repository;

use App\Entity\InvoiceClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InvoiceClient>
 *
 * @method InvoiceClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvoiceClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvoiceClient[]    findAll()
 * @method InvoiceClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvoiceClient::class);
    }

//    /**
//     * @return InvoiceClient[] Returns an array of InvoiceClient objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?InvoiceClient
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
