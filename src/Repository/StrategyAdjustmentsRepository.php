<?php

namespace App\Repository;

use App\Entity\StrategyAdjustments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StrategyAdjustments>
 *
 * @method StrategyAdjustments|null find($id, $lockMode = null, $lockVersion = null)
 * @method StrategyAdjustments|null findOneBy(array $criteria, array $orderBy = null)
 * @method StrategyAdjustments[]    findAll()
 * @method StrategyAdjustments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StrategyAdjustmentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StrategyAdjustments::class);
    }

//    /**
//     * @return StrategyAdjustments[] Returns an array of StrategyAdjustments objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?StrategyAdjustments
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
