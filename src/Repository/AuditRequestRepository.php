<?php

namespace App\Repository;

use App\Entity\AuditRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AuditRequest>
 *
 * @method AuditRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuditRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuditRequest[]    findAll()
 * @method AuditRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuditRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuditRequest::class);
    }

//    /**
//     * @return AuditRequest[] Returns an array of AuditRequest objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?AuditRequest
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
