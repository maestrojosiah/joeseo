<?php

namespace App\Twig;

use App\Manager\CartManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Repository\UserRepository;
use App\Repository\ProjectRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class GlobalVars extends AbstractExtension
{
    public function __construct(private readonly UserRepository $usr, private readonly ProjectRepository $projectRepo, private readonly Security $security)
    {
    }

    public function getFunctions(): ?array
    {
        return [new TwigFunction('global_vars', $this->getGlobalVars(...))];
    }

    public function getGlobalVars($select)
    {
        $projects = $this->projectRepo->findBy(
            ['project_status' => ['in_progress', 'completed']],
            ['start_date' => 'ASC']
        );

        $user = $this->getUserFromSecurityContext();

        $url = parse_url((string) $_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['projects'] = $projects;
        // $glVars['cart'] = $this->cartManager->getCurrentCart($user);
        
        // $glVars['thisUrl'] = $url['path'];

        return $glVars[$select];

    }

     /**
     * Gets the currently logged-in user from the security context.
     *
     * @return UserInterface|null
     */
    private function getUserFromSecurityContext(): ?UserInterface
    {
        $token = $this->getSecurityToken();

        if ($token && $token->getUser() instanceof UserInterface) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Gets the security token from the security context.
     *
     * @return TokenInterface|null
     */
    private function getSecurityToken(): ?TokenInterface
    {
        return $this->security->getToken();
    }


}
