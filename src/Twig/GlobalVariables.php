<?php

namespace App\Twig;

use App\Controller\Admin\DashboardController;
use App\Manager\CartManager;
use App\Repository\BlogRepository;
use App\Repository\SEORepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ProductRepository;
use App\Entity\Orderr;
use App\Repository\OrderrRepository;
use App\Repository\PageRepository;
use App\Repository\ConfigurationRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class GlobalVariables extends AbstractExtension
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator, private PageRepository $pageRepository, private UserRepository $usr, private ConfigurationRepository $settingRepo, private SEORepository $seoRepo, private Security $security, private BlogRepository $blogRepository)
    {
    }

    public function getFunctions(): ?array
    {
        return [new TwigFunction('global_variables', $this->getGlobalVars(...))];
    }

    public function getGlobalVars($select)
    {
        $seo = $this->seoRepo->findAll();
        $setting = $this->settingRepo->findAll();
        $user = $this->getUserFromSecurityContext();
        $blog_posts = $this->blogRepository->findPublished(true, 3);
        $pages = $this->pageRepository->findByIsService(true);
    
        $glVars = [];
        $glVars['seo'] = !empty($seo) ? $seo[0] : null;
        $glVars['setting'] = !empty($setting) ? $setting[0] : null;
        $glVars['posts'] = $blog_posts;
        $glVars['pages'] = $pages;
        
        if ($user && $this->security->isGranted('ROLE_ADMIN')) {
            // Add edit links for admins
            if(!empty($seo)){
                $glVars['seo_edit_link'] = sprintf(
                    '<a target="_blank" href="%s"><i class="icofont-pencil"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\SEOCrudController') // Adjust controller accordingly
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($seo[0]->getId()) // Assuming the first SEO entity
                        ->generateUrl()
                );
    
            }
            if(!empty($setting)){
                $glVars['setting_edit_link'] = sprintf(
                    '<a target="_blank" href="%s"><i class="icofont-pencil"></i></a>',
                    $this->adminUrlGenerator
                        ->setController('App\\Controller\\Admin\\SettingCrudController') // Adjust controller accordingly
                        ->setDashboard(DashboardController::class)
                        ->setAction('edit')
                        ->setEntityId($setting[0]->getId()) // Assuming the first Setting entity
                        ->generateUrl()
                );
            }
            
        }
    
        return $glVars[$select];
    }

    /**
     * Gets the currently logged-in user from the security context.
     *
     * @return UserInterface|null
     */
    private function getUserFromSecurityContext(): ?UserInterface
    {
        $token = $this->getSecurityToken();

        if ($token && $token->getUser() instanceof UserInterface) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Gets the security token from the security context.
     *
     * @return TokenInterface|null
     */
    private function getSecurityToken(): ?TokenInterface
    {
        return $this->security->getToken();
    }


}