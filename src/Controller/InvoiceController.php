<?php

namespace App\Controller;

use App\Controller\Admin\InvoiceCrudController;
use App\Controller\Admin\InvoiceUserDashboardController;
use App\Repository\InvoiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\AuditRequest;
use App\Entity\Email;
use App\Entity\InvoiceItem;
use App\Form\AuditType;
use App\Repository\BlogRepository;
use App\Repository\PricingRepository;
use App\Repository\ProjectRepository;
use App\Repository\TemplateRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use App\Service\SeoService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Controller\SecurityController;
use App\Entity\Company;
use App\Entity\Invoice;
use App\Entity\InvoiceClient;
use App\Entity\Notes;
use App\Entity\Theme;
use App\Form\RegistrationFormType;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class InvoiceController extends AbstractController
{

    public function __construct(private TemplateRepository $templateRepo, private readonly SeoService $seoService, private readonly EntityManagerInterface $em, private readonly Mailer $mailer, private readonly UserRepository $userRepo, private InvoiceRepository $invoiceRepo, private AdminUrlGenerator $adminUrlGenerator){}

    #[Route('/invoice/template/{template_name}', name: 'preview_template_by_name')]
    public function preview_template(Request $request, $template_name, AuthenticationUtils $authenticationUtils): Response
    {
        $i_n = $request->query->get('i-n');

        if($i_n && null !== $this->getUser()) {
            return $this->redirectToRoute('invoice_generator', [
                'template_name' => $template_name,
                'i-n' => $i_n 
            ]);
        }


        // Handle SEO tags as before
        $this->seoService->setTitle('Simple Invoice Generator: Easy, Free, and Professional')
            ->setDescription("Invoicing solutions designed to elevate your Kenyan online business...")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi...')
            ->setOgTitle('Simple Invoice Generator: Easy, Free, and Professional')
            ->setOgDescription("Invoicing solutions designed to elevate your Kenyan online business...")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');
    
        // Get login form data
        $securityController = new SecurityController($this->seoService);
        $loginForm = $securityController->renderLoginForm($authenticationUtils);
        $registrationForm = $this->createForm(RegistrationFormType::class);

        return $this->render('html_templates/' . $template_name . '.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'template_name' => $template_name,
            'registrationForm' => $registrationForm->createView(),
            'last_username' => $loginForm["last_username"],
            'error' => $loginForm["error"],
        ]);
    }
    
    #[Route('/save/invoice/number', name: 'save_invoice_number', methods: ['POST'])]
    #[IsGranted('ROLE_USER')] // Ensure the user is logged in and has at least the ROLE_USER role
    public function saveInvoiceNumber(Request $request, EntityManagerInterface $entityManager, TemplateRepository $templateRepo): Response
    {
        $invoiceNumber = $request->request->get('invoiceNumber');
        $template_name = $request->request->get('template_name');

        if ($invoiceNumber) {

            $existingInvoice = $this->invoiceRepo->findOneBy([
                'invoiceNumber' => $invoiceNumber,
                'user' => $this->getUser()
            ]);

            // if there is an existing invoice
            if(null !== $existingInvoice){
                
                $invoice = $existingInvoice;

                $theme = $invoice->getTheme();

                if(!$theme){
                    $theme = new Theme();
                    $theme->setName("Rename This Theme");
                    $theme->setPriColor("#CDCDCD");
                    $theme->setSecColor("#F1F1F2");
                    $theme->setUser($this->getUser());
                    $invoice->setTheme($theme);
                }

                // give it the current theme
                $template = $templateRepo->findOneByName($template_name);
                $theme->setTemplate($template);
                $invoice->setTheme($theme);
    
                $entityManager->persist($invoice);
                $entityManager->flush();

                return $this->redirectToRoute('invoice_generator', [
                    'template_name' => $template_name,
                    'i-n' => $invoiceNumber 
                ]);
            }

            $invoice = new Invoice();
            $invoice->setInvoiceNumber($invoiceNumber);
            $invoice->setUser($this->getUser());
            $invoice->setType("invoice");

            // Assuming other necessary fields are set or defaulted as needed
            // $invoice->setOtherField(...);
            $client = new InvoiceClient();
            $client->setName("Client's Full Name");
            $client->setUser($this->getUser());
            $invoice->setClient($client);
            
            $company = new Company();
            $company->setPhone("+254");
            $company->setUser($this->getUser());
            $client->setCompany($company);
                
            $theme = new Theme();
            $theme->setName("Theme-" . date('ymd'));
            $theme->setPriColor("#CDCDCD");
            $theme->setSecColor("#F1F1F2");
            $theme->setUser($this->getUser());
            $invoice->setTheme($theme);
            
            $template = $templateRepo->findOneByName($template_name);
            $theme->setTemplate($template);
        
            $notes = new Notes();
            $notes->setUser($this->getUser());
            $notes->setDescription("This is a computer generated invoice.");
            $invoice->setNotes($notes);
                
            // Persist the changes
            $entityManager->persist($invoice);
            $entityManager->persist($client); 
            $entityManager->persist($company);
            $entityManager->persist($theme); 
            $entityManager->persist($notes);
    
            $entityManager->flush();

            // Optionally, add a flash message or redirect
            // $this->addFlash('success', 'Invoice number saved successfully.');

            // Redirect back to the same page or wherever you want
            return $this->redirectToRoute('invoice_generator', [
                'template_name' => $template_name,
                'i-n' => $invoiceNumber 
            ]);
        }

        // Handle error or invalid data
        // $this->addFlash('error', 'Failed to save invoice number.');

        return $this->redirectToRoute('preview_template_by_name', [
            'template_name' => $template_name
        ]);
    }

    #[Route('/invoice/builder/kenya/free', name: 'old_invoice_link')]
    public function oldLink(): RedirectResponse
    {
        return $this->redirectToRoute('app_invoice_index', [], 301);
    }

    #[Route('/invoice/builder/easy-and-free', name: 'app_invoice_index')]
    public function index(ProjectRepository $projectRepo, BlogRepository $blogRepo, Request $request): Response
    {
        $form = $this->createForm(AuditType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->saveAudit($form->getData());
        } 

        $projects = $projectRepo->findNotOnHold();
        $blogs = $blogRepo->findBy(
            [],
            ['id' => 'DESC'],
            3
        );

        $templates = $this->templateRepo->findAll();

        $this->seoService->setTitle('Simple Invoice Generator: Easy, Free, and Professional')
            ->setDescription("Invoicing solutions designed to elevate your Kenyan online business & drive tangible results. Discover your business's potential with JoeSEO, Nairobi's premier SEO company.")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Simple Invoice Generator: Easy, Free, and Professional')
            ->setOgDescription("Invoicing solutions designed to elevate your Kenyan online business & drive tangible results. Discover your business's potential with JoeSEO, Nairobi's premier SEO company.")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('index/invoice_builder.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'projects' => $projects,
            'templates' => $templates,
            'blogs' => $blogs,
            'form'=> $form,
            'admin_url'=> 'admin_invoice'
        ]);
        
    }

    #[Route('/invoice/generator/preview/{template_name}', name: 'invoice_generator', defaults: ['template_name' => ''])]
    public function invoice_generator(InvoiceRepository $invoiceRepo, Request $request, $template_name): Response
    {
        $invoiceNumber = $request->query->get('i-n');

        if (!$this->isGranted('ROLE_USER')) {
            $invoiceNumberExists = $invoiceRepo->findOneByInvoiceNumber($invoiceNumber);

            if ($invoiceNumberExists) {
        
                $theme = $invoiceNumberExists->getTheme();
                $template = $theme->getTemplate();
        
                if($template){
                    return $this->redirectToRoute('preview_template_by_name', ['template_name' => $template->getName(), 'i-n' => $invoiceNumber]);
                } else {
                    return $this->redirectToRoute('app_login');
                }
        
            }
        }

        $invoice = $invoiceRepo->findOneBy(
            ['invoiceNumber' => $invoiceNumber, 'user' => $this->getUser()],
            ['id' => 'desc']        
        );
        
        $invoiceIndexUrl = $this->adminUrlGenerator
            ->setController(InvoiceCrudController::class)
            ->setAction('index')
            ->setDashboard(InvoiceUserDashboardController::class)
            ->generateUrl();

        if (!$invoice) {
            $this->seoService->setTitle("Build Your Invoice And Download It For free")
                ->setDescription("Invoicing solutions designed to elevate your Kenyan online business & drive tangible results. Discover your business's potential with JoeSEO, Nairobi's premier SEO company.")
                ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
                ->setOgTitle("Build Your Invoice And Download It For free")
                ->setOgDescription("IInvoicing solutions designed to elevate your Kenyan online business & drive tangible results. Discover your business's potential with JoeSEO, Nairobi's premier SEO company.")
                ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
                ->setOgUrl('https://joeseo.co.ke/')
                ->setOgType('website');

            return $this->render('html_templates/' . $template_name . '.html.twig', [
                'meta_tags' => $this->seoService->generateMetaTags(),
            ]);
        }

        $template = $invoice->getTheme()->getTemplate();

        $this->seoService->setTitle($invoice->getClient()->getName() . ' | Invoice Number ' . $invoice->getInvoiceNumber())
            ->setDescription("Invoicing solutions designed to elevate your Kenyan online business & drive tangible results. Discover your business's potential with JoeSEO, Nairobi's premier SEO company.")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle($invoice->getClient()->getName() . ' Generated Invoice: Invoice Number ' . $invoiceNumber)
            ->setOgDescription("IInvoicing solutions designed to elevate your Kenyan online business & drive tangible results. Discover your business's potential with JoeSEO, Nairobi's premier SEO company.")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('index/' . $template->getTemplateName(), [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'invoice' => $invoice,
            'invoiceIndexUrl' => $invoiceIndexUrl
        ]);
    }

    #[Route('/invoice/update', name: 'update_invoice', methods: ['POST'])]
    #[IsGranted('ROLE_USER')] // Ensure the user is logged in and has at least the ROLE_USER role
    public function updateInvoice(Request $request, InvoiceRepository $invoiceRepository, EntityManagerInterface $entityManager): JsonResponse
    {
        // Retrieve the invoice number from the request
        $invoiceNumber = $request->get('invoiceNumber');
        
        // Fetch the invoice to update using the invoice number
        $invoice = $invoiceRepository->findOneBy(
            ['invoiceNumber' => $invoiceNumber, 'user' => $this->getUser()],
            ['id' => 'desc']        
        );
    
        if (!$invoice) {
            return new JsonResponse(['error' => 'Invoice not found.'], 404);
        }    
        
        // Update the invoice fields
        $invoice->setDate(new \DateTimeImmutable($request->get('invoiceDate')));

        $client = $invoice->getClient();
        $client->setName($request->get('clientName'));
        $client->setContactDetails($request->get('clientContactDetails'));
        $client->setAddress($request->get('clientAddress'));
        $client->setEmail($request->get('clientEmail'));
        $client->setPhone($request->get('clientPhone'));
        $client->setWebsite($request->get('clientWebsite'));

        $company = $client->getCompany();
        if(null == $company) {
            $company = new Company();
            $company->setCompanyName($request->get('myCompany'));
            $client->setCompany($company);
        } else {
            $company->setCompanyName($request->get('myCompany'));
            $client->setCompany($company);
        }

        $company->setLogoType($request->get('companyLogoType'));
        $company->setWebsite($request->get('companyWebsite'));
        $company->setEmail($request->get('companyEmail'));
        $company->setPhone($request->get('companyPhone'));
        $company->setUser($invoice->getUser());
    
        // Handle the logo file upload
        $logoFile = $request->files->get('companyLogo');
        if ($logoFile) {
            $newFilename = uniqid().'.'.$logoFile->guessExtension();
    
            // Move the file to the directory where logos are stored
            $logoFile->move(
                $this->getParameter('company_logo_directory'), // Set this parameter in your config
                $newFilename
            );
    
            // Update the logo path in the company entity
            $company->setLogoPath($newFilename);
        }
    
        $theme = $invoice->getTheme();
        $theme->setPriColor($request->get('primaryColor'));
        $theme->setSecColor($request->get('secondaryColor'));
        $theme->setUser($invoice->getUser());
    
        $notes = $invoice->getNotes();
        $notes->setUser($invoice->getUser());
        $notes->setDescription($request->get('notes'));
    
        $invoice->setPaymentInfo($request->get('paymentInfo'));
        $invoice->setTaxType($request->get('taxType'));
        $invoice->setTotalAmount($request->get('totalAmount'));
        $invoice->setTax($request->get('taxAmount'));
        $invoice->setDiscount($request->get('discount'));
        $invoice->setTermsConditions($request->get('termsConditions'));
        $invoice->setType($request->get('invoiceType'));
    
        // Persist the changes
        $entityManager->persist($invoice);
        $entityManager->persist($client); 
        $entityManager->persist($company);
        $entityManager->persist($theme); 
        $entityManager->persist($notes);
    
        // Flush to save to the database
        $entityManager->flush();
    
        return new JsonResponse(['success' => 'Invoice updated successfully.']);
    }
        
    #[Route('/invoice/item/save', name: 'invoice_item_save', methods: ['POST'])]
    public function saveInvoiceItem(Request $request, EntityManagerInterface $em): Response
    {
        $data = $request->request->all();
        $invoiceItemId = $data['id'] ?? null;
        $invoiceItem = $invoiceItemId ? $em->getRepository(\App\Entity\InvoiceItem::class)->find($invoiceItemId) : new InvoiceItem();

        if (!$invoiceItem) {
            return $this->json(['status' => 'error', 'message' => 'Invoice item not found.'], Response::HTTP_NOT_FOUND);
        }

        $invoice = $em->getRepository(\App\Entity\Invoice::class)->find($data['invoice_id']);
        $invoiceItem->setDescription($data['description']);
        $invoiceItem->setQuantity((int)$data['quantity']);
        $invoiceItem->setUnitPrice((float)$data['unit_price']);
        $invoiceItem->setTotalAmount((float)$data['total_amount']);
        $invoiceItem->setUser($this->getUser());
        $invoiceItem->setInvoice($invoice);

        $em->persist($invoiceItem);
        $em->flush();

        return $this->json(['status' => 'success', 'id' => $invoiceItem->getId()]);
    }

    #[Route('/invoice/item/delete', name: 'invoice_item_delete', methods: ['POST'])]
    public function deleteInvoiceItem(Request $request, EntityManagerInterface $em): Response
    {
        $itemId = $request->request->get('id');
        $invoiceItem = $em->getRepository(\App\Entity\InvoiceItem::class)->find($itemId);

        if ($invoiceItem) {
            $em->remove($invoiceItem);
            $em->flush();
            return $this->json(['status' => 'success']);
        }

        return $this->json(['status' => 'error', 'message' => 'Invoice item not found.'], Response::HTTP_NOT_FOUND);
    }

    // #[Route('/invoice/item/add', name: 'invoice_item_add')]
    // public function addInvoiceItem(Request $request, EntityManagerInterface $em): JsonResponse
    // {
    //     $description = $request->request->get('description');
    //     $unitPrice = $request->request->get('unitPrice');
    //     $quantity = $request->request->get('quantity');
    //     $totalAmount = $request->request->get('totalAmount');

    //     $invoiceItem = new InvoiceItem();
    //     $invoiceItem->setDescription($description);
    //     $invoiceItem->setUnitPrice($unitPrice);
    //     $invoiceItem->setQuantity($quantity);
    //     $invoiceItem->setTotalAmount($totalAmount);

    //     $em->persist($invoiceItem);
    //     $em->flush();

    //     return new JsonResponse(['success' => true, 'id' => $invoiceItem->getId()]);
    // }

    // #[Route('/invoice/item/update', name: 'invoice_item_update')]
    // public function updateInvoiceItem(Request $request, EntityManagerInterface $em): JsonResponse
    // {
    //     $id = $request->request->get('id');
    //     $description = $request->request->get('description');
    //     $unitPrice = $request->request->get('unitPrice');
    //     $quantity = $request->request->get('quantity');
    //     $totalAmount = $request->request->get('totalAmount');

    //     $invoiceItem = $em->getRepository(\App\Entity\InvoiceItem::class)->find($id);
    //     if (!$invoiceItem) {
    //         return new JsonResponse(['success' => false, 'message' => 'Item not found']);
    //     }

    //     $invoiceItem->setDescription($description);
    //     $invoiceItem->setUnitPrice($unitPrice);
    //     $invoiceItem->setQuantity($quantity);
    //     $invoiceItem->setTotalAmount($totalAmount);

    //     $em->flush();

    //     return new JsonResponse(['success' => true]);
    // }

    // #[Route('/invoice/item/delete', name: 'invoice_item_delete')]
    // public function deleteInvoiceItem(Request $request, EntityManagerInterface $em): JsonResponse
    // {
    //     $id = $request->request->get('id');

    //     $invoiceItem = $em->getRepository(\App\Entity\InvoiceItem::class)->find($id);
    //     if (!$invoiceItem) {
    //         return new JsonResponse(['success' => false, 'message' => 'Item not found']);
    //     }

    //     $em->remove($invoiceItem);
    //     $em->flush();

    //     return new JsonResponse(['success' => true]);
    // }    

    private function saveAudit($data)
    {

        $auditRequest = new AuditRequest();

        $audit = $data;
        $name = $audit['name'];
        $email = $audit['email'];
        $phone = $audit['phone'];
        $website = $audit['website'];
        $company_name = $audit['company_name'];
        $date = new \DateTimeImmutable();

        $auditRequest->setName($name);
        $auditRequest->setEmail($email);
        $auditRequest->setPhone($phone);
        $auditRequest->setWebsite($website);
        $auditRequest->setCompanyName($company_name);
        $auditRequest->setDateRequested($date);

        $this->em->persist($audit);
        $this->em->flush();

        $subject = "Audit Request";
        $admins = $this->userRepo->findByUsertype('admin');
        $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => "Audit Requested by $name for $website, $company_name. Phone: $phone, "];

        foreach ($admins as $admin) {
            $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
        }
        $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");

        $this->addFlash('success','Your Message has been sent');
        return $this->redirectToRoute('app_index');

    }    

}
