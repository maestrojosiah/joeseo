<?php

namespace App\Controller;

use App\Service\SeoService;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(private readonly SeoService $seoService){}

    #[Route(path: '/security/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
    
        $this->seoService->setTitle('Login to JoeSEO')
            ->setDescription("JoeSEO is a trusted SEO agency in Nairobi, Kenya...")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi...')
            ->setOgTitle('Login to JoeSEO')
            ->setOgDescription("JoeSEO is a trusted SEO agency in Nairobi, Kenya...")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');
    
        return $this->render('security/login.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }    

    #[Route(path: '/security/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    public function renderLoginForm(AuthenticationUtils $authenticationUtils): array
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
    
        return [
            'last_username' => $lastUsername,
            'error' => $error,
        ];
    }
        
}


