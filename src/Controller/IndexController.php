<?php

namespace App\Controller;

use App\Entity\AuditRequest;
use App\Entity\Email;
use App\Form\AuditType;
use App\Repository\BlogRepository;
use App\Repository\PricingRepository;
use App\Repository\ProjectRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SeoService;

class IndexController extends AbstractController
{
    public function __construct(private readonly SeoService $seoService, private readonly EntityManagerInterface $em, private readonly Mailer $mailer, private readonly UserRepository $userRepo){}

    #[Route('/seo/services/nairobi', name: 'seo_services_nairobi')]
    public function seoNairobi(PricingRepository $pricingRepo, ProjectRepository $projectRepo, BlogRepository $blogRepo, Request $request): Response
    {


        $form = $this->createForm(AuditType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->saveAudit($form->getData());
        } 

        $pricings = $pricingRepo->findAll();
        $projects = $projectRepo->findNotOnHold();
        $blogs = $blogRepo->findBy(
            [],
            ['id' => 'DESC'],
            3
        );
        
        $this->seoService->setTitle('Leading SEO Services in Nairobi - Joe SEO Consultants')
            ->setDescription("Discover your business's potential with JoeSEO, Nairobi's premier SEO company. SEO solutions designed to elevate your online presence & drive tangible results.")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Leading SEO Services in Nairobi - Joe SEO Consultants')
            ->setOgDescription("Discover your business's potential with JoeSEO, Nairobi's premier SEO company. SEO solutions designed to elevate your online presence & drive tangible results.")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('index/seo-nairobi.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'pricings' => $pricings,
            'projects' => $projects,
            'blogs' => $blogs,
            'form'=> $form,
            'admin_url'=> 'admin'
        ]);
    }


    #[Route('/show/profile', name: 'app_profile_show')]
    public function profile(): Response
    {
        return $this->render('index/profile.html.twig', [
            'test' => 'Test',
        ]);

    }

    #[Route('/keyword/checker', name: 'keyword_checker')]
    public function keywordChecker(Request $request): Response
    {
        $keywords = trim($request->query->get('keywords'));

        $this->seoService->setTitle('Check Keyword Density For Your Content')
            ->setDescription("Would you like to be sure if you are targeting the right keywords? Find out if your article/content is using all the keywords that you intend to use.")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Check Keyword Density For Your Content')
            ->setOgDescription("Would you like to be sure if you are targeting the right keywords? Find out if your article/content is using all the keywords that you intend to use.")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('index/article_keywords.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'keywords' => $keywords,
        ]);

    }

    #[Route('/keywords/checker/add', name: 'keywords_add')]
    public function keywords(): Response
    {
        $this->seoService->setTitle('Free Keyword Density Checker For Your Content')
            ->setDescription("Would you like to be sure if you are targeting the right keywords? Find out if your article/content is using all the keywords that you intend to use.")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Free Keyword Density Checker For Your Content')
            ->setOgDescription("Would you like to be sure if you are targeting the right keywords? Find out if your article/content is using all the keywords that you intend to use.")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('index/keywords.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
        ]);

    }

    private function saveAudit($data)
    {

        $auditRequest = new AuditRequest();

        $audit = $data;
        $name = $audit['name'];
        $email = $audit['email'];
        $phone = $audit['phone'];
        $website = $audit['website'];
        $company_name = $audit['company_name'];
        $date = new \DateTimeImmutable();

        $auditRequest->setName($name);
        $auditRequest->setEmail($email);
        $auditRequest->setPhone($phone);
        $auditRequest->setWebsite($website);
        $auditRequest->setCompanyName($company_name);
        $auditRequest->setDateRequested($date);

        $this->em->persist($audit);
        $this->em->flush();

        $subject = "Audit Request";
        $admins = $this->userRepo->findByUsertype('admin');
        $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => "Audit Requested by $name for $website, $company_name. Phone: $phone, "];

        foreach ($admins as $admin) {
            $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
        }
        $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");

        $this->addFlash('success','Your Message has been sent');
        return $this->redirectToRoute('app_index');

    }    

    #[Route(path: '/test/test/test', name: 'multitesting')]
    public function testingmultiplethings(): Response
    {

        $arr = ["src/Controller/InvoiceController.php", "src/Controller/RegistrationController.php", "templates/admin/fields/link.html.twig", "templates/admin/inv_index.html.twig", "templates/index/invoice_form.html.twig", "templates/index/it.html.twig", "templates/index/restaurant.html.twig", "templates/inv_base.html.twig", "templates/inv_base_no_var.html.twig"];

        $proj_dir = $this->getParameter('proj_dir');
        $test = [];
        foreach($arr as $a) {
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }

        return $this->render('index/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }

}
