<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use App\Security\UserAuthenticator;
use App\Service\SeoService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private EmailVerifier $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier, private readonly SeoService $seoService)
    {
        $this->emailVerifier = $emailVerifier;
    }

    #[Route('/security/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, UserAuthenticator $authenticator, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if (isset($_POST['username']) && !empty($_POST['username'])) {
                // Redirect to https://example.com
                header("Location: https://example.com");
                exit(); // Ensure that no further code is executed after the redirect
            }
            
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address('contact@joeseo.co.ke', 'Joe SEO'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            // do anything else you need here, like send an email

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );

            // Check for the _target_path and redirect if it exists
            $targetPath = $request->get('_target_path');
            if ($targetPath) {
                return new RedirectResponse($targetPath);
            }

        }

        $this->seoService->setTitle('Sign Up to start your journey to success')
            ->setDescription("JoeSEO is a trusted SEO agency in Nairobi, Kenya. We are ROI focused and so confident in our ability to deliver.")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Sign Up to start your journey to success')
            ->setOgDescription("JoeSEO is a trusted SEO agency in Nairobi, Kenya. We are ROI focused and so confident in our ability to deliver.")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('registration/register.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/security/verify/email', name: 'app_verify_email')]
    public function verifyUserEmail(Request $request, TranslatorInterface $translator, UserRepository $userRepository): Response
    {
        $id = $request->query->get('id');

        if (null === $id) {
            return $this->redirectToRoute('app_register');
        }

        $user = $userRepository->find($id);

        if (null === $user) {
            return $this->redirectToRoute('app_register');
        }

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $translator->trans($exception->getReason(), [], 'VerifyEmailBundle'));

            return $this->redirectToRoute('app_register');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirectToRoute('app_register');
    }
}
