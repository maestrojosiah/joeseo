<?php

namespace App\Controller\Admin;

use App\Entity\Configuration;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ConfigurationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Configuration::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Settings");
        yield TextField::new("business_name")->setLabel("Business Name");
        yield TextField::new("phone_number")->setLabel("Phone Number");
        yield TextField::new("email_address")->setLabel("Email Address");
        yield TextField::new("work_hours")->setLabel("Work Days and Hours")
            ->hideOnIndex();
        yield TextField::new("business_location")->setLabel("Business Location")
            ->hideOnIndex();
        yield TextField::new("google_map_embed")->setLabel("Google Map Embed Code")
            ->hideOnIndex();

        yield FormField::addColumn(6)->setLabel("Social Media")
            ->hideOnIndex();
        yield TextField::new("instagram_link")->setLabel("Instagram URL")
            ->hideOnIndex();
        yield TextField::new("facebook_link")->setLabel("Facebook URL")
            ->hideOnIndex();
        yield TextField::new("youtube_link")->setLabel("Youtube URL")
            ->hideOnIndex();
        yield TextField::new("tiktok_link")->setLabel("Tiktok URL")
            ->hideOnIndex();
        yield TextField::new("twitter_link")->setLabel("Twitter URL")
            ->hideOnIndex();
        yield TextField::new("linkedin_link")->setLabel("Linkedin URL")
            ->hideOnIndex();
    }
    
}
