<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if ($this->isGranted('ROLE_ADMIN')) {
            return $queryBuilder;
        }

        return $queryBuilder
            ->andWhere('entity.id = :id')
            ->setParameter('id', $this->getUser()->getId());

    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInSingular("My Profile")
            ->setEntityLabelInPlural("My Profile");
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new User();
        $template->setRegistrationDate(new \DateTimeImmutable());

        return $template;
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextField::new("fullName")->setLabel("Full Name")
            ->hideOnForm();
        yield TextField::new("firstName")->setLabel("First Name")
            ->onlyOnForms();
        yield TextField::new("lastName")->setLabel("Last Name")
            ->onlyOnForms();
        yield EmailField::new("email")->setLabel("Email");
        yield ChoiceField::new("usertype")->setLabel("Usertype")
            ->setChoices([
                'User' => 'user',
                'Admin' => 'admin',
            ])
            ->setPermission('ROLE_ADMIN');
        yield ChoiceField::new("gender")->setLabel("Gender")
            ->setChoices([
                'Male' => 'male',
                'Female' => 'female',
            ]);
        yield ImageField::new("profilePicture")->setLabel("Photo")
            ->setUploadDir("/public/site/images/profile_pictures")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/profile_pictures");
        yield BooleanField::new("isActive")->setLabel("Is Active?")
            ->setPermission('ROLE_ADMIN');
        yield BooleanField::new("isVerified")->setLabel("Is Verified?")
            ->setPermission('ROLE_ADMIN');
        $roles = ['ROLE_ADMIN', 'ROLE_USER'];
        yield ChoiceField::new('roles')
            ->setChoices(array_combine($roles, $roles))
            ->allowMultipleChoices()
            ->renderAsBadges()
            ->renderExpanded()
            ->setPermission('ROLE_ADMIN');
        yield TextField::new('password')
            ->setRequired($pageName === Crud::PAGE_NEW)
            ->onlyWhenCreating();

    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
        ->setPermission(Action::NEW, 'ROLE_ADMIN');
    }

}
