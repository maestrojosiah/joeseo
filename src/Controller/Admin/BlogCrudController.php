<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BlogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Blog::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Blog();
        $template->setAuthor($this->getUser());
        $template->setPublicationDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setPublicationDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("author")->setLabel("Author")
            ->hideOnForm();
        yield AssociationField::new("categories")->setLabel("Blog Categories")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete();

        yield TextField::new("title")->setLabel("Title");
        yield TextField::new("slug")->setLabel("Slug");
        yield TextField::new("tags")->setLabel("Tags")
            ->onlyOnForms();
        yield ImageField::new("image_url")->setLabel("Image URI")
        ->setUploadDir("/public/site/images/blog_images")
        ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
        ->setBasePath("/site/images/blog_images")
        ->onlyOnForms();
        yield TextField::new("video_embed")->setLabel("Video Embed")
            ->onlyOnForms();
        yield TextareaField::new("content")->setLabel("Content")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
            
        yield DateField::new("publication_date")->setLabel("Publication Date")
            ->hideOnForm();
        yield ChoiceField::new("status")->setLabel("Blog Status")
            ->setChoices([
                'Draft' => 'draft',
                'Published' => 'published',
            ]);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
