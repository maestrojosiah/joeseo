<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Client::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Client();
        $template->setCreationDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setCreationDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("user_id")->setLabel("User")
            ->autocomplete();
        yield TextField::new("company_name")->setLabel("Company Name");
        yield TextField::new("contact_person")->setLabel("Contact Person");
        yield TextField::new("contact_email")->setLabel("Contact Email");
        yield TextField::new("contact_phone")->setLabel("Contact Phone");
        yield TextField::new("website_url")->setLabel("Website Url");
        yield ChoiceField::new("pref_contact_method")->setLabel("Preferred Contact Method")
            ->setChoices([
                'Email' => 'emaail',
                'Text Message' => 'sms',
                'Call' => 'call',
            ]);
        yield TextareaField::new("notes")->setLabel("Notes")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

        yield DateField::new("last_contact_date")->setLabel("Last Contact Date");
        yield DateField::new("creation_date")->setLabel("Creation Date")
            ->hideOnForm();

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }

}
