<?php

namespace App\Controller\Admin;

use App\Entity\InvoiceClient;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class InvoiceClientCrudController extends AbstractCrudController
{
    public function __construct( private EntityRepository $entityManager, private AdminUrlGenerator $adminUrlGenerator)
    {}

    public static function getEntityFqcn(): string
    {
        return InvoiceClient::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $client = new InvoiceClient();
        $client->setUser($this->getUser());

        return $client;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }

    
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {

        yield TextField::new("name")->setLabel("Client Name")
            ->setHelp("Client's name"); 
        yield TextField::new("contactDetails")->setLabel("Client's Business")
            ->setHelp("Client's company name or business details");
        yield TextField::new("address")->setLabel("Address")
            ->hideOnIndex()
                ->setHelp("Client's physical/postal adress");
        yield TextField::new("email")->setLabel("Email")
            ->setHelp("Client's email address");
        yield TextField::new("phone")->setLabel("Phone")
            ->setHelp("Client's phone number");
        yield AssociationField::new("company")->setLabel("Company")
            ->hideOnIndex()
            ->setHelp("Which of your companies is this client associated to?");
        yield AssociationField::new("user")->setLabel("User")
            ->onlyOnDetail();

    }

    public function configureActions(Actions $actions): Actions
    {

        $templatesUrl = $this->adminUrlGenerator
            ->setController(TemplateCrudController::class)
            ->setAction(Crud::PAGE_INDEX)
            ->setDashboard(InvoiceUserDashboardController::class)
            ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $sendInvoice = Action::new('goToTemplates', 'Templates', 'fa fa-arrow-right')
            ->addCssClass('btn btn-info')
            ->linkToUrl($templatesUrl);

        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $sendInvoice)
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }
    
    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInSingular("Client")
            ->setEntityLabelInPlural("Clients");
    }

}
