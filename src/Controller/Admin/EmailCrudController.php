<?php

namespace App\Controller\Admin;

use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class EmailCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Email::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("sender")->setLabel("Sender")
            ->autocomplete();
        yield AssociationField::new("project")->setLabel("Project")
            ->autocomplete();
        yield TextField::new("recipient_email")->setLabel("Recipient Email");
        yield TextField::new("subject")->setLabel("Subject");
        yield TextareaField::new("body")->setLabel("Message")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

        yield DateField::new("date_sent")->setLabel("Date Sent:");
        yield ChoiceField::new("status")->setLabel("Status")
            ->setChoices([
                'Sent' => 'sent',
                'Delivered' => 'delivered',
                'Read' => 'read',
            ]);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }

}
