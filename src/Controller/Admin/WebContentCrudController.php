<?php

namespace App\Controller\Admin;

use App\Entity\WebContent;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\RequestStack;

class WebContentCrudController extends AbstractCrudController
{

    public function __construct(private RequestStack $requestStack)
    {
        
    }

    public function createEntity(string $entityFqcn)
    {
        $content = new WebContent();

        
    
        // Prefill the slug and contentText if passed as query parameters
        $slug = $this->getSlug();
        if ($slug) {
            $content->setDescription($slug);
        }
    
        $contentText = $this->getContentText();
        if ($contentText) {
            $content->setContentText($contentText);
        }
    
        return $content;
    }

    public function edit(AdminContext $context)
    {
        // Get the current entity being edited
        $entityInstance = $context->getEntity()->getInstance();

        // Pass the entity instance to the configureFields method
        return parent::edit($context);
    }

    public function getSlug(){
        $request = $this->requestStack->getCurrentRequest();
        $slug = $request->query->get('slug');
        return $slug;
    }

    public function getContentText(){
        $request = $this->requestStack->getCurrentRequest();
        $slug = $request->query->get('contentText');
        return $slug;
    }

    public static function getEntityFqcn(): string
    {
        return WebContent::class;
    }


    public function configureFields(string $pageName, $entityInstance = null): iterable
    {
        $entityInstance = $this->getContext()->getEntity()->getInstance();

        yield FormField::addColumn(6)->setLabel("Content Information");
        yield TextField::new("title")->setLabel("Title");
        yield TextField::new("description")->setLabel("Slug");
        // Ensure entity instance is available
        $content_text = $entityInstance ? $entityInstance->getContentText() : 'nothing';
        // Check if the content contains HTML tags
        if(null !== $content_text){
            if ($this->containsHtml($content_text)) {
                // Render with CKEditor if HTML is present
                yield TextareaField::new("content_text")->setLabel("Text")
                    ->hideOnIndex()
                    ->setFormTypeOptions([
                        'attr' => ['class' => 'ckeditor'],
                    ])
                    ->setHelp("<pre>" . htmlspecialchars($content_text) . "</pre>");
            } else {
                // Render as plain textarea if no HTML is present
                yield TextareaField::new("content_text")->setLabel("Text")
                    ->hideOnIndex()
                    ->setFormTypeOptions([
                        'attr' => ['class' => 'plain-textarea'],
                    ]);
            }
    
        } else {
            yield TextareaField::new("content_text")->setLabel("Text")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'plain-textarea'],
            ]);

        }
    }

    private function containsHtml(string $content): bool
    {
        // Returns true if content has HTML tags, false otherwise
        return $content !== strip_tags($content);
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
