<?php

namespace App\Controller\Admin;

use App\Entity\Keyword;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class KeywordCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Keyword::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Keyword();
        $template->setCreationDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setCreationDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("project")->setLabel("Project")
            ->autocomplete();
        yield TextField::new("keyword")->setLabel("Keyword");
        yield IntegerField::new("search_volume")->setLabel("Search Volume");
        yield IntegerField::new("current_ranking")->setLabel("Current Ranking");
        yield IntegerField::new("target_ranking")->setLabel("Target Ranking");
        yield ChoiceField::new("keyword_difficulty")->setLabel("Keyword Difficulty")
            ->setChoices([
                'Easy' => '1',
                'Medium' => '2',
                'Hard' => '3',
            ]);
        yield ChoiceField::new("keyword_type")->setLabel("Keyword Type")
            ->setChoices([
                'Primary' => 'primary',
                'Secondary' => 'seconday',
                'Long Tail' => 'long_tail',
            ]);
        yield TextareaField::new("notes")->setLabel("Notes")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

        yield DateField::new("creation_date")->setLabel("Creation Date")
            ->hideOnForm();
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }

}
