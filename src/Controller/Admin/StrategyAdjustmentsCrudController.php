<?php

namespace App\Controller\Admin;

use App\Entity\StrategyAdjustments;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class StrategyAdjustmentsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StrategyAdjustments::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new StrategyAdjustments();
        $template->setAdjustmentDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setAdjustmentDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }



    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("project")->setLabel("Project")
            ->autocomplete();
        yield DateField::new("adjustment_date")->setLabel("Date Adjusted")
            ->hideOnForm();
        yield TextareaField::new("adjustment_details")->setLabel("Adjustment Details")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }

}
