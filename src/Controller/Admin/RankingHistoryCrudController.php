<?php

namespace App\Controller\Admin;

use App\Entity\RankingHistory;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class RankingHistoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RankingHistory::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new RankingHistory();
        $template->setRankingDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setRankingDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("keyword")->setLabel("Keyword")
            ->autocomplete();
        yield DateField::new("ranking_date")->setLabel("Date")
            ->hideOnForm();
        yield IntegerField::new("ranking_position")->setLabel("Position");
    }

}
