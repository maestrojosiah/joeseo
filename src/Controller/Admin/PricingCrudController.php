<?php

namespace App\Controller\Admin;

use App\Entity\Pricing;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PricingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Pricing::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("services")->setLabel("Services")
            ->setFormTypeOption('by_reference', false)
            ->autocomplete()
            ->hideOnDetail();
        yield CollectionField::new("services")->setLabel("Services")
            ->onlyOnDetail()
            ->setTemplatePath('admin/fields/services.html.twig');
        yield IntegerField::new("price")->setLabel("Price");
        yield TextField::new("currency")->setLabel("Currency");
        yield ChoiceField::new("pricing_category")->setLabel("Pricing Category")
            ->setChoices([
                'Basic' => 'basic',
                'Standard' => 'standard',
                'Premium' => 'premium',
            ]);
        yield ChoiceField::new("billing_cycle")->setLabel("Billing Cycle")
            ->setChoices([
                'Monthly' => 'montly',
                'Yearly' => 'yearly',
            ]);
    }

}
