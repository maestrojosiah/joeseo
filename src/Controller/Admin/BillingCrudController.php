<?php

namespace App\Controller\Admin;

use App\Entity\Billing;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class BillingCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Billing::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $billing = new Billing();
        $billing->setInvoiceDate(new \DateTimeImmutable());

        return $billing;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setInvoiceDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("project")->setLabel("Project")
            ->autocomplete();
        yield AssociationField::new("client")->setLabel("Client")
            ->autocomplete();
        yield TextField::new("transaction_id")->setLabel("Transaction ID");
        yield IntegerField::new("amount")->setLabel("Amount")
            ->onlyOnForms();
        yield TextareaField::new("notes")->setLabel("Notes")
                ->hideOnIndex()
                ->setFormType(CKEditorType::class);
        yield DateField::new("invoice_date")->setLabel("Invoice Date")
            ->hideOnForm();
        yield DateField::new("due_date")->setLabel("Due Date");
        yield ChoiceField::new("payment_status")->setLabel("Payment Status")
            ->setChoices([
                'Paid' => 'paid',
                'Unpaid' => 'unpaid',
            ]);
        yield ChoiceField::new("payment_method")->setLabel("Payment Method")
            ->setChoices([
                'Mpesa' => 'mpesa',
                'Bank' => 'bank',
                'Cheque' => 'cheque',
                'Cash' => 'cash',
            ]);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
