<?php

namespace App\Controller\Admin;

use App\Entity\Content;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ContentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Content::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Content();
        $template->setAuthor($this->getUser());
        $template->setCreationDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setCreationDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("project")->setLabel("Project")
            ->autocomplete();
        yield TextField::new("content_title")->setLabel("Content Title")
            ->onlyOnForms();
        yield IntegerField::new("word_count")->setLabel("Word Count")
            ->onlyOnForms();
        yield AssociationField::new("author")->setLabel("Author")
            ->autocomplete();
        yield DateField::new("creation_date")->setLabel("Creation Date")
            ->hideOnForm();
        yield ChoiceField::new("content_type")->setLabel("Content Type")
            ->setChoices([
                'Article' => 'article',
                'Blog' => 'blog',
            ]);
        yield ChoiceField::new("content_status")->setLabel("Content Status")
            ->setChoices([
                'Draft' => 'draft',
                'Published' => 'published',
            ]);
        yield TextareaField::new("notes")->setLabel("Notes")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
