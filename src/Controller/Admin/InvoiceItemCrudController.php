<?php

namespace App\Controller\Admin;

use App\Entity\InvoiceItem;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class InvoiceItemCrudController extends AbstractCrudController
{
    public function __construct( private EntityRepository $entityManager, private AdminUrlGenerator $adminUrlGenerator)
    {}

    public static function getEntityFqcn(): string
    {
        return InvoiceItem::class;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = $this->entityManager->createQueryBuilder($searchDto, $entityDto, $fields, $filters);

        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
            $qb->andWhere('entity.user = :user');
        } else {
            $qb->andWhere('entity.user = :user');
        }

        $qb->setParameter('user', $this->getUser());

        return $qb;

        // return $this->container->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new InvoiceItem();
        $template->setUser($this->getUser());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setUser($this->getUser());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Basic Info");

        yield TextField::new("description")->setLabel("Description")
            ->setHelp("Description of the service or item.");
        yield IntegerField::new("quantity")->setLabel("Quantity")
            ->setHelp("Quantity of the described item.");
        yield IntegerField::new("unitPrice")->setLabel("Unit Price")
            ->setHelp("Unit price of a single service or item.");
        yield IntegerField::new("totalAmount")->setLabel("Total Amount")
            ->setHelp("Total amount - (quantity x unit price).");
        yield AssociationField::new("invoice")->setLabel("Invoice")
            ->setHelp("Select the invoice which you're adding this item to.");
        yield AssociationField::new("user")->setLabel("User")
        ->onlyOnDetail();

    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setEntityLabelInSingular("Invoice Item")
            ->setEntityLabelInPlural("Invoice Items");
    }

    // add a button on top 'Add a note to your invoice'
    public function configureActions(Actions $actions): Actions
    {

        // $notesUrl = $this->adminUrlGenerator
        //     ->setRoute("app_index", ['e-n' => $this->getContext()->getEntity()->getInstance()->getId()])
        //     ->generateUrl();

        // if the method is not defined in a CRUD controller, link to its route
        $preview = Action::new('preview', 'Preview Invoice', 'fa fa-eye')
            ->addCssClass('btn btn-info')
            ->linkToUrl(function ($entity) {
                // Assuming getId() is the method to get the ID of your entity
                $entityId = $entity->getInvoice()->getInvoiceNumber();
    
                if ($entityId !== null) {
                    return $this->generateUrl('invoice_generator', ['en' => $entityId]);
                }
    
                // Handle the case where the entity ID is null
                return '#'; // or any other fallback URL
            });
    
        return $actions
        // ...
        ->add(Crud::PAGE_INDEX, $preview)
        // ->remove(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER)
        ;
        
    }

}
