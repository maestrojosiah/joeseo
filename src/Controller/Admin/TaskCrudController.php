<?php

namespace App\Controller\Admin;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class TaskCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Task::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Task();
        $template->setCreationDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setCreationDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    
    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("project")->setLabel("Project")
            ->autocomplete();
        yield TextField::new("taskDescription")->setLabel("Task Description")
            ->onlyOnForms();
        yield AssociationField::new("assigned_to")->setLabel("Assigned To:")
            ->autocomplete()
            ->onlyOnForms();
        yield DateField::new("deadline")->setLabel("Deadline");
        yield ChoiceField::new("taskStatus")->setLabel("Task Status")
            ->setChoices([
                'ToDo' => 'user',
                'In Progress' => 'in_progress',
                'Completed' => 'completed',
            ]);
        yield ChoiceField::new("taskPriority")->setLabel("Task Priority")
            ->setChoices([
                'High' => 'high',
                'Medium' => 'medium',
                'Low' => 'low',
            ]);
        yield TextareaField::new("notes")->setLabel("Notes")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        yield DateField::new("creationDate")->setLabel("Created On:");
        yield DateField::new("startDate")->setLabel("Created On:");
        yield ChoiceField::new("taskDuration")->setLabel("Duration in Days")
            ->setChoices([
                '1 day' => '1',
                '2 days' => '2',
                '3 days' => '3',
                '4 days' => '4',
                '5 days' => '5',
                '6 days' => '6',
                '7 days' => '7',
                '8 days' => '8',
                '9 days' => '9',
                '10 days' => '10',
                '15 days' => '15',
                '20 days' => '20',
                '25 days' => '25',
                '30 days' => '30',
            ]);
            yield ChoiceField::new("month")->setLabel("Month")
            ->setChoices([
                'Month 1' => '1',
                'Month 2' => '2',
                'Month 3' => '3',
                'Month 4' => '4',
                'Month 5' => '5',
                'Month 6' => '6',
                'Month 7' => '7',
                'Month 8' => '8',
                'Month 9' => '9',
                'Month 10' => '10',
                'Month 11' => '11',
                'Month 12' => '12',
            ]);

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
