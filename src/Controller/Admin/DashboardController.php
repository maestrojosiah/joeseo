<?php

namespace App\Controller\Admin;

use App\Entity\AuditRequest;
use App\Entity\Billing;
use App\Entity\Blog;
use App\Entity\BlogCategory;
use App\Entity\BlogComment;
use App\Entity\Client;
use App\Entity\CommentReply;
use App\Entity\Configuration;
use App\Entity\Content;
use App\Entity\Email;
use App\Entity\Image;
use App\Entity\Keyword;
use App\Entity\Page;
use App\Entity\Partner;
use App\Entity\Pricing;
use App\Entity\Project;
use App\Entity\RankingHistory;
use App\Entity\Report;
use App\Entity\Section;
use App\Entity\SEO;
use App\Entity\Service;
use App\Entity\Setting;
use App\Entity\StrategyAdjustments;
use App\Entity\Task;
use App\Entity\Testimonials;
use App\Entity\User;
use App\Entity\WebContent;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use LogicException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('JoeSEO')
            ->setFaviconPath('/site/assets/img/favicon.ico');
            ;
    }

    public function configureMenuItems(): iterable
    {
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        $user = $this->getUser();

        if(!$user instanceof User){
            return new LogicException('Not user');
        }

        if ('admin' === $user->getUsertype()) {
            //     return $this->redirect('...');
                yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');

                yield Menuitem::section('Blog');
                yield MenuItem::linkToCrud('Blog', 'fas fa-pen', Blog::class);
                yield MenuItem::linkToCrud('Blog Category', 'fas fa-folder', BlogCategory::class);
                yield MenuItem::linkToCrud('Blog Comment', 'fas fa-comment', BlogComment::class);
                yield MenuItem::linkToCrud('Comment Replies', 'fas fa-comments', CommentReply::class);

                yield Menuitem::section('User');
                yield MenuItem::linkToCrud('User', 'fas fa-users', User::class);
                yield MenuItem::linkToCrud('Client', 'fas fa-user', Client::class);

                yield Menuitem::section('Web Content');
                yield MenuItem::linkToCrud('Text Content', 'fas fa-globe', WebContent::class);
                yield MenuItem::linkToCrud('Image Content', 'fas fa-image', Image::class);
                yield MenuItem::linkToCrud('Sections', 'fas fa-columns', Section::class);
                yield MenuItem::linkToCrud('Pages', 'fas fa-file', Page::class);
        
                yield Menuitem::section('Settings');
                yield MenuItem::linkToCrud('Basic Settings', 'fas fa-gear', Setting::class);
                yield MenuItem::linkToCrud('SEO Settings', 'fas fa-search', SEO::class);
                yield MenuItem::linkToCrud('Website Configuration', 'fas fa-search', Configuration::class);
                yield MenuItem::linkToCrud('Settings', 'fas fa-gear', Setting::class);
        
                yield Menuitem::section('Messages');
                yield MenuItem::linkToCrud('Emails', 'fa fa-envelope-o', Email::class);
                yield MenuItem::linkToCrud('Audit Request', 'fas fa-eye', AuditRequest::class);

                yield Menuitem::section('Website');
                yield MenuItem::linkToCrud('Billing', 'fa fa-money', Billing::class);
                yield MenuItem::linkToCrud('Content', 'fas fa-file', Content::class);
                yield MenuItem::linkToCrud('Keyword', 'fas fa-key', Keyword::class);
                yield MenuItem::linkToCrud('Pricing', 'fa fa-money', Pricing::class);
                yield MenuItem::linkToCrud('Project', 'fas fa-tasks', Project::class);
                yield MenuItem::linkToCrud('Ranking History', 'fas fa-clock', RankingHistory::class);
                yield MenuItem::linkToCrud('Reports', 'fas fa-chart-bar', Report::class);
                yield MenuItem::linkToCrud('Service', 'fas fa-cogs', Service::class);
                yield MenuItem::linkToCrud('Strategy Adjustments', 'fas fa-bullseye', StrategyAdjustments::class);
                yield MenuItem::linkToCrud('Task', 'fas fa-tasks', Task::class);
                yield MenuItem::linkToCrud('Partners', 'fas fa-users', Partner::class);
                yield MenuItem::linkToCrud('Testimonials', 'fas fa-comment', Testimonials::class);
                
                yield Menuitem::section('Others');
                yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_index'));

        } else {
            // yield MenuItem::linkToCrud('User', 'fas fa-users', User::class);
            yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');

        }
    
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL) ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        if (!$user instanceof User) {
            throw new \Exception('Wrong user');
        }

        return parent::configureUserMenu($user)
            ->setAvatarUrl("/site/images/profile_pictures/".$user->getAvatarUri())
            ->setMenuItems([
                MenuItem::linkToUrl('My Profile','fas fa-user', $this->generateUrl('app_profile_show')),
                MenuItem::linkToUrl('Logout', 'fa fa-sign-out', $this->generateUrl('app_logout')),
            ]);

    }

    // public function configureAssets(): Assets
    // {
    //     return parent::configureAssets()
    //         ->addCssFile('/site/css/test.css'); 
    // }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->setDefaultSort(
                ['id' => 'DESC']
            );
    }    

}
