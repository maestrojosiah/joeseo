<?php

namespace App\Controller\Admin;

use App\Entity\AuditRequest;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class AuditRequestCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AuditRequest::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextField::new("name")->setLabel("Full Name");
        yield TextField::new("email")->setLabel("Email");
        yield TextField::new("phone")->setLabel("Phone");
        yield TextField::new("website")->setLabel("Website");
        yield TextField::new("company_name")->setLabel("Company Name");
        yield DateField::new("date_requested")->setLabel("Date");
    }

}
