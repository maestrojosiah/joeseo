<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class ProjectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Project();
        $template->setCreationDate(new \DateTimeImmutable());

        return $template;
    }

    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setCreationDate(new \DateTimeImmutable());

        $entityManager->persist($entityInstance);
        $entityManager->flush();
    }

    

    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("client")->setLabel("Client")
        ->hideOnIndex()
        ->autocomplete();
        yield AssociationField::new("project_manager")->setLabel("Project Manager")
            ->autocomplete()
            ->hideOnIndex();
        yield TextField::new("project_name")->setLabel("Project Name");
        yield DateField::new("start_date")->setLabel("Start Date")
            ->hideOnIndex();
        yield DateField::new("end_date")->setLabel("End Date")
            ->hideOnIndex();
        yield DateField::new("creation_date")->setLabel("Creation Date")
            ->hideOnIndex();
        yield ChoiceField::new("project_status")->setLabel("Project Status")
            ->setChoices([
                'In Progress' => 'in_progress',
                'Completed' => 'completed',
                'On Hold' => 'on_hold',
            ]);
        yield IntegerField::new('budget')->setLabel('Budget');
        yield ChoiceField::new('priority_level')->setLabel('Priority Level')
            ->setChoices([
                'High' => 'high',
                'Medium' => 'medium',
                'Low' => 'low'
            ]);
        yield ChoiceField::new('project_type')->setLabel('Project Type')
            ->setChoices([
                'SEO' => 'SEO',
                'Web Development' => 'Web Development',
                'SEO & Web Development' => 'SEO & Web Development',
                'Content Marketing' => 'Content Marketing',
            ]);
        yield ImageField::new("project_image")->setLabel("Project Image [640x450]")
            ->setUploadDir("/public/site/images/project_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/project_images");
        yield TextareaField::new("project_description")->setLabel("Project Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);


    }

    
    // public function configureCrud(Crud $crud): Crud
    // {
    //     return $crud
    //         ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
    //         ->showEntityActionsInlined()
    //     ;
    // }
    
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
