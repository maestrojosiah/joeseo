<?php

namespace App\Controller\Admin;

use App\Entity\BlogCategory;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;

class BlogCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BlogCategory::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield ChoiceField::new("category_name")->setLabel("Category Name")
            ->setChoices([
                "SEO"=> "SEO",
                "Google Business Profile"=> "Google_Business_Profile",
                "Web Development"=> "Web_Development",
            ]);
    }

}
