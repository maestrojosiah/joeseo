<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use App\Service\SeoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    public function __construct(private SeoService $seoService, private ProjectRepository $projectRepo)
    {
        
    }
    // #[Route('/projects', name: 'app_project')]
    // public function index(): Response
    // {
    //     $projects = $this->projectRepo->findBy(
    //         ['project_status' => ['in_progress', 'completed']],
    //         ['start_date' => 'ASC']
    //     );
    //     $types = [];

    //     foreach ($projects as $project) {
    //         $typeKey = preg_replace('/[^a-zA-Z0-9]/', '_', $project->getProjectType());
    //         $types[$typeKey] = $project->getProjectType();
    //     }

    //     return $this->render('project/index.html.twig', [
    //         'types' => $types,
    //         'projects' => $projects
    //     ]);
    // }

    #[Route('/project/details/{name}', name: 'project_detail')]
    public function project_details($name): Response
    {
        $project = $this->projectRepo->findOneByProjectName($name);

        $this->seoService->setTitle($project->getProjectType() . " Project | " . $project->getClient()->getCompanyName())
            ->setDescription(substr(strip_tags($project->getProjectDescription()), 0, 155) . "...")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle($project->getProjectType() . " Project | Joe SEO Consultancy")
            ->setOgDescription(substr($project->getProjectDescription(), 0, 155) . "...")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('project/detail.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'project' => $project,
        ]);

    }

}
