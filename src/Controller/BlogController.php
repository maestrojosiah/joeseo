<?php

namespace App\Controller;

use App\Repository\BlogCategoryRepository;
use App\Repository\BlogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SeoService;

class BlogController extends AbstractController
{
    public function __construct(private SeoService $seoService){}

    #[Route('/blog', name: 'app_blog')]
    public function index(BlogRepository $blogRepo, BlogCategoryRepository $blogCategoryRepo): Response
    {
        $recentBlogs = $blogRepo->findBy(
            [],
            ['id' => 'DESC'],
            10
        );

        $posts = $blogRepo->findByStatus('published');

        $this->seoService->setTitle('Handy SEO Tips For Your Business | Joe SEO')
            ->setDescription('Easy-to-understand SEO and GBP tips to make your business stand out on search engines. Manage your back-end like a pro!')
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Handy SEO Tips For Your Business')
            ->setOgDescription('Easy-to-understand SEO and GBP tips to make your business stand out on search engines. Manage your back-end like a pro!')
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');
        
        return $this->render('blog/index.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'posts' => $posts,
            'blogCategories' => $blogCategoryRepo->findAll(),
            'recentBlogs' => $recentBlogs,
        ]);
    }

    #[Route('/blog/category/{category}', name: 'app_blog_category')]
    public function indexCategory(BlogRepository $blogRepo, BlogCategoryRepository $blogCategoryRepo, $category): Response
    {
        $recentBlogs = $blogRepo->findBy(
            [],
            ['id' => 'DESC'],
            10
        );

        $posts = $blogRepo->createQueryBuilder('b')
            ->innerJoin('b.categories', 'c')
            ->where('c.category_name = :categoryName')
            ->andWhere('b.status = :status')
            ->setParameter('categoryName', $category)
            ->setParameter('status', 'published')
            ->orderBy('b.id', 'DESC')
            ->getQuery()
            ->getResult();

        $this->seoService->setTitle('Handy SEO Tips For Your Business | ' . str_replace('_', ' ', $category))
            ->setDescription('Easy-to-understand ' . str_replace('_', ' ', $category) . ' tips to make your business stand out on search engines. Manage your back-end like a pro!')
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle('Handy SEO Tips For Your Business | ' . str_replace('_', ' ', $category))
            ->setOgDescription('Easy-to-understand ' . str_replace('_', ' ', $category) . ' tips to make your business stand out on search engines. Manage your back-end like a pro!')
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('blog/index.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'posts' => $posts,
            'recentBlogs' => $recentBlogs,
            'blogCategories' => $blogCategoryRepo->findAll(),
        ]);
    }

    #[Route('/blog/{slug}', name: 'blog_detail')]
    public function show(BlogRepository $blogRepo, BlogCategoryRepository $blogCategoryRepo, $slug): Response
    {
        $recentBlogs = $blogRepo->findBy(
            [],
            ['id' => 'DESC'],
            10
        );

        $post = $blogRepo->findOneBySlug($slug);

        $this->seoService->setTitle($post->getTitle())
            ->setDescription(substr(strip_tags($post->getContent()), 0,150) . "...")
            ->setKeywords('SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya')
            ->setOgTitle($post->getTitle())
            ->setOgDescription(substr(strip_tags($post->getContent()), 0,150) . "...")
            ->setOgImage('https://joeseo.co.ke/img/joe-seo-logo.jpg')
            ->setOgUrl('https://joeseo.co.ke/')
            ->setOgType('website');

        return $this->render('blog/detail.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'post' => $post,
            'recentBlogs' => $recentBlogs,
            'blogCategories' => $blogCategoryRepo->findAll(),
        ]);
    }
}
