<?php

namespace App\Controller;

use App\Entity\AuditRequest;
use App\Form\AuditType;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogRepository;
use App\Repository\WebContentRepository;
use App\Repository\FAQRepository;
use App\Repository\ImageRepository;
use App\Repository\PageRepository;
use App\Repository\PartnerRepository;
use App\Repository\PricingRepository;
use App\Repository\ProjectRepository;
use App\Repository\SEORepository;
use App\Repository\TestimonialsRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Service\SeoService;
use Symfony\Component\HttpFoundation\RequestStack;

class DefaultController extends AbstractController
{

    public function __construct(
        private ProjectRepository $projectRepo, 
        private SEORepository $seo_settings, 
        private RequestStack $requestStack, 
        private UserRepository $userRepo, 
        private SeoService $seoService, 
        private EntityManagerInterface $em, 
        private readonly Mailer $mailer
    ){}

    #[Route('/', name: 'app_index')]
    public function index(PartnerRepository $partnerRepo, PricingRepository $pricingRepo, ProjectRepository $projectRepo, BlogRepository $blogRepository, FAQRepository $fAQRepository, TestimonialsRepository $testimonialsRepository, Request $request, PageRepository $pageRepository): Response
    {
        $form = $this->createForm(AuditType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->saveAudit($form->getData());
        } 

        $pricings = $pricingRepo->findAll();
        $projects = $projectRepo->findNotOnHold();
        $blogs = $blogRepository->findBy(
            [],
            ['id' => 'DESC'],
            3
        );

        $partners = $partnerRepo->findAll();

        $index = $pageRepository->findOneByUrl('home-page');
        $services = $pageRepository->findByIsService(true);
        $testimonials = $testimonialsRepository->findAll();
        // $blog_posts = $blogRepository->findPublished(true, 3);
        $faqs = $fAQRepository->findBy(
            [],
            ['id' => 'asc'],
            3
        );

        // $this->getTags(null, null);
        $this->getTags($index->getMetaTitle(), $index->getMetaDescription());

        return $this->render('index/index.html.twig', [
            'page' => $index,
            'services' => $services,
            'testimonials' => $testimonials,
            'faqs' => $faqs,
            'partners' => $partners,
            'meta_tags' => $this->seoService->generateMetaTags(),
            'pricings' => $pricings,
            'projects' => $projects,
            'blogs' => $blogs,
            'form'=> $form,
            'admin_url'=> 'admin'

        ]);
    }

    public function getTags($title, $description, $keywords = null, $image_path = null){

        $currentRequest = $this->requestStack->getCurrentRequest();
        $currentUrl = $currentRequest->getUri(); // Get the full current URL
        $seoSettings = $this->seo_settings->findOneBy([], ['id' => 'ASC']);

        $seo_title = null == $seoSettings ? 'Customized SEO Services in Kenya - Trusted SEO Services Nairobi.' : $seoSettings->getMetaTitle();
        $seo_description = null == $seoSettings ? 'JoeSEO is a trusted SEO agency in Nairobi, Kenya. We are ROI focused and so confident in our ability to deliver.' : $seoSettings->getMetaDescription();
        $seo_keywords = null == $seoSettings ? 'SEO services Nairobi, Best SEO companies in Nairobi, Affordable SEO Kenya, SEO for small businesses Nairobi, Kenyan SEO agency, Increase website traffic Kenya, Improve Google ranking Nairobi, SEO consultant Nairobi, Local SEO Nairobi, E-commerce SEO Nairobi, Content marketing for SEO Nairobi, Link building services Kenya, Mobile SEO Nairobi, Technical SEO audit Kenya, Website optimization Nairobi, Google My Business optimization Kenya' : $seoSettings->getKeywords();

        $this->seoService->setTitle($title == null ? $seo_title : $title)
        ->setDescription($description == null ? $seo_description : $description)
        ->setKeywords($keywords == null ? $seo_keywords : $keywords)
        ->setOgTitle($title == null ? $seo_title : $title)
        ->setOgDescription($description == null ? $seo_description : $description)
        ->setOgImage($image_path == null ? 'https://joeseo.co.ke/img/joe-seo-logo.jpg' : $image_path)
        ->setOgUrl($currentUrl)
        ->setOgType('website');

    }    

    #[Route('/{page}', name: 'app_page')]
    public function page(Request $request, PartnerRepository $partnerRepo, TestimonialsRepository $testimonialsRepository, BlogRepository $blogRepository, FAQRepository $fAQRepository, WebContentRepository $contentRepository, PageRepository $pageRepository, ImageRepository $imageRepository, $page): Response
    {

        $form = $this->createForm(AuditType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->saveAudit($form->getData());
        } 

        $projects = $this->projectRepo->findBy(
            ['project_status' => ['in_progress', 'completed']],
            ['start_date' => 'ASC']
        );
        $types = [];

        foreach ($projects as $project) {
            $typeKey = preg_replace('/[^a-zA-Z0-9]/', '_', $project->getProjectType());
            $types[$typeKey] = $project->getProjectType();
        }

        // Other unchanged code...
        $page = $pageRepository->findOneByUrl($page);
        $partners = $partnerRepo->findAll();
        $services = $pageRepository->findByIsService(true);
        $testimonials = $testimonialsRepository->findAll();
        $all_images = $imageRepository->findAll();
        $title = $page->getHeading();
        $page_category = "Pages";
        $faqs = $fAQRepository->findAll();
        $blog_posts = $blogRepository->findPublished('published', 50);
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        $this->getTags($page->getMetaTitle(), $page->getMetaDescription());

        return $this->render('index/page.html.twig', [
            'meta_tags' => $this->seoService->generateMetaTags(),
            'page_category' => $page_category,
            'page' => $page,
            'services' => $services,
            'partners' => $partners,
            'types' => $types,
            'projects' => $projects,
            'images' => $images,
            'form' => $form,
            'title' => $title,
            'faqs' => $faqs,
            'testimonials' => $testimonials,
            'blog_posts' => $blog_posts,
        ]);
    }

    #[Route('/service/{service}', name: 'app_service')]
    public function service(WebContentRepository $contentRepository, PageRepository $pageRepository, $service): Response
    {
        $content = [];
        $service_page = $pageRepository->findOneByUrl($service);
        $services = $pageRepository->findByIsService(true);

       return $this->render('index/service.html.twig', [
            'page' => $service_page,
            'services' => $services,
        ]);
    }

    private function saveAudit($data)
    {

        $auditRequest = new AuditRequest();

        $audit = $data;
        $name = $audit['name'];
        $email = $audit['email'];
        $phone = $audit['phone'];
        $website = $audit['website'];
        $company_name = $audit['company_name'];
        $date = new \DateTimeImmutable();

        $auditRequest->setName($name);
        $auditRequest->setEmail($email);
        $auditRequest->setPhone($phone);
        $auditRequest->setWebsite($website);
        $auditRequest->setCompanyName($company_name);
        $auditRequest->setDateRequested($date);

        $this->em->persist($audit);
        $this->em->flush();

        $subject = "Audit Request";
        $admins = $this->userRepo->findByUsertype('admin');
        $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => "Audit Requested by $name for $website, $company_name. Phone: $phone, "];

        foreach ($admins as $admin) {
            $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
        }
        $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");

        $this->addFlash('success','Your Message has been sent');
        return $this->redirectToRoute('app_index');

    }    



}
