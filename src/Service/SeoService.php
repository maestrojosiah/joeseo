<?php 

namespace App\Service;

class SeoService
{
    private $title;
    private $description;
    private $keywords;
    private $ogTitle;
    private $ogDescription;
    private $ogImage;
    private $ogUrl;
    private $ogType;

    public function __construct()
    {
        $this->title = '';
        $this->description = '';
        $this->keywords = '';
        $this->ogTitle = '';
        $this->ogDescription = '';
        $this->ogImage = '';
        $this->ogUrl = '';
        $this->ogType = 'website'; // Default type
    }

    // SEO Meta Tags
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function setKeywords(string $keywords): self
    {
        $this->keywords = $keywords;
        return $this;
    }

    // Open Graph Meta Tags
    public function setOgTitle(string $ogTitle): self
    {
        $this->ogTitle = $ogTitle;
        return $this;
    }

    public function setOgDescription(string $ogDescription): self
    {
        $this->ogDescription = $ogDescription;
        return $this;
    }

    public function setOgImage(string $ogImage): self
    {
        $this->ogImage = $ogImage;
        return $this;
    }

    public function setOgUrl(string $ogUrl): self
    {
        $this->ogUrl = $ogUrl;
        return $this;
    }

    public function setOgType(string $ogType): self
    {
        $this->ogType = $ogType;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getKeywords(): string
    {
        return $this->keywords;
    }

    public function getOgTitle(): string
    {
        return $this->ogTitle;
    }

    public function getOgDescription(): string
    {
        return $this->ogDescription;
    }

    public function getOgImage(): string
    {
        return $this->ogImage;
    }

    public function getOgUrl(): string
    {
        return $this->ogUrl;
    }

    public function getOgType(): string
    {
        return $this->ogType;
    }

    public function generateMetaTags(): string
    {
        $metaTags = '';

        // Standard SEO tags
        if (!empty($this->title)) {
            $metaTags .= '<title>' . htmlspecialchars($this->title) . '</title>' . "\n";
        }

        if (!empty($this->description)) {
            $metaTags .= '<meta name="description" content="' . htmlspecialchars($this->description) . '">' . "\n";
        }

        if (!empty($this->keywords)) {
            $metaTags .= '<meta name="keywords" content="' . htmlspecialchars($this->keywords) . '">' . "\n";
        }

        // Open Graph tags
        if (!empty($this->ogTitle)) {
            $metaTags .= '<meta property="og:title" content="' . htmlspecialchars($this->ogTitle) . '">' . "\n";
        }

        if (!empty($this->ogDescription)) {
            $metaTags .= '<meta property="og:description" content="' . htmlspecialchars($this->ogDescription) . '">' . "\n";
        }

        if (!empty($this->ogImage)) {
            $metaTags .= '<meta property="og:image" content="' . htmlspecialchars($this->ogImage) . '">' . "\n";
        }

        if (!empty($this->ogUrl)) {
            $metaTags .= '<meta property="og:url" content="' . htmlspecialchars($this->ogUrl) . '">' . "\n";
        }

        $metaTags .= '<meta property="og:type" content="' . htmlspecialchars($this->ogType) . '">' . "\n";

        return $metaTags;
    }
}
