<?php

namespace App\Entity;

use App\Repository\CommentReplyRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CommentReplyRepository::class)]
class CommentReply
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'commentReplies')]
    private ?BlogComment $comment = null;

    #[ORM\ManyToOne(inversedBy: 'commentReplies')]
    private ?User $user_id = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $content = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $reply_date = null;

    public function __toSTring()
    {
        return (string)$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?BlogComment
    {
        return $this->comment;
    }

    public function setComment(?BlogComment $comment): static
    {
        $this->comment = $comment;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): static
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getReplyDate(): ?\DateTimeImmutable
    {
        return $this->reply_date;
    }

    public function setReplyDate(?\DateTimeImmutable $reply_date): static
    {
        $this->reply_date = $reply_date;

        return $this;
    }
}
