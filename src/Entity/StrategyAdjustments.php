<?php

namespace App\Entity;

use App\Repository\StrategyAdjustmentsRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StrategyAdjustmentsRepository::class)]
class StrategyAdjustments
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'strategyAdjustments')]
    private ?Project $project = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $adjustment_date = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $adjustment_details = null;

    public function __toSTring()
    {
        return (string)$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;

        return $this;
    }

    public function getAdjustmentDate(): ?\DateTimeImmutable
    {
        return $this->adjustment_date;
    }

    public function setAdjustmentDate(?\DateTimeImmutable $adjustment_date): static
    {
        $this->adjustment_date = $adjustment_date;

        return $this;
    }

    public function getAdjustmentDetails(): ?string
    {
        return $this->adjustment_details;
    }

    public function setAdjustmentDetails(?string $adjustment_details): static
    {
        $this->adjustment_details = $adjustment_details;

        return $this;
    }
}
