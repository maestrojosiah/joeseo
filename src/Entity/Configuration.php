<?php

namespace App\Entity;

use App\Repository\ConfigurationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConfigurationRepository::class)]
class Configuration
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $business_name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $phone_number = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $work_hours = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $instagram_link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $facebook_link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $youtube_link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tiktok_link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $business_location = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $twitter_link = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $linkedin_link = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $google_map_embed = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $email_address = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBusinessName(): ?string
    {
        return $this->business_name;
    }

    public function setBusinessName(?string $business_name): static
    {
        $this->business_name = $business_name;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): static
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getWorkHours(): ?string
    {
        return $this->work_hours;
    }

    public function setWorkHours(?string $work_hours): static
    {
        $this->work_hours = $work_hours;

        return $this;
    }

    public function getInstagramLink(): ?string
    {
        return $this->instagram_link;
    }

    public function setInstagramLink(?string $instagram_link): static
    {
        $this->instagram_link = $instagram_link;

        return $this;
    }

    public function getFacebookLink(): ?string
    {
        return $this->facebook_link;
    }

    public function setFacebookLink(?string $facebook_link): static
    {
        $this->facebook_link = $facebook_link;

        return $this;
    }

    public function getYoutubeLink(): ?string
    {
        return $this->youtube_link;
    }

    public function setYoutubeLink(?string $youtube_link): static
    {
        $this->youtube_link = $youtube_link;

        return $this;
    }

    public function getTiktokLink(): ?string
    {
        return $this->tiktok_link;
    }

    public function setTiktokLink(?string $tiktok_link): static
    {
        $this->tiktok_link = $tiktok_link;

        return $this;
    }

    public function getBusinessLocation(): ?string
    {
        return $this->business_location;
    }

    public function setBusinessLocation(?string $business_location): static
    {
        $this->business_location = $business_location;

        return $this;
    }

    public function getTwitterLink(): ?string
    {
        return $this->twitter_link;
    }

    public function setTwitterLink(?string $twitter_link): static
    {
        $this->twitter_link = $twitter_link;

        return $this;
    }

    public function getLinkedinLink(): ?string
    {
        return $this->linkedin_link;
    }

    public function setLinkedinLink(?string $linkedin_link): static
    {
        $this->linkedin_link = $linkedin_link;

        return $this;
    }

    public function getGoogleMapEmbed(): ?string
    {
        return $this->google_map_embed;
    }

    public function setGoogleMapEmbed(?string $google_map_embed): static
    {
        $this->google_map_embed = $google_map_embed;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function setEmailAddress(?string $email_address): static
    {
        $this->email_address = $email_address;

        return $this;
    }

}
