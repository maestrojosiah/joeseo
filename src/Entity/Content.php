<?php

namespace App\Entity;

use App\Repository\ContentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContentRepository::class)]
class Content
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'contents')]
    private ?Project $project = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $content_title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $content_type = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $creation_date = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $content_status = null;

    #[ORM\ManyToOne(inversedBy: 'contents')]
    private ?User $author = null;

    #[ORM\Column(nullable: true)]
    private ?int $word_count = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    public function __toSTring()
    {
        return $this->content_title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;

        return $this;
    }

    public function getContentTitle(): ?string
    {
        return $this->content_title;
    }

    public function setContentTitle(?string $content_title): static
    {
        $this->content_title = $content_title;

        return $this;
    }

    public function getContentType(): ?string
    {
        return $this->content_type;
    }

    public function setContentType(?string $content_type): static
    {
        $this->content_type = $content_type;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeImmutable
    {
        return $this->creation_date;
    }

    public function setCreationDate(?\DateTimeImmutable $creation_date): static
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    public function getContentStatus(): ?string
    {
        return $this->content_status;
    }

    public function setContentStatus(?string $content_status): static
    {
        $this->content_status = $content_status;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;

        return $this;
    }

    public function getWordCount(): ?int
    {
        return $this->word_count;
    }

    public function setWordCount(?int $word_count): static
    {
        $this->word_count = $word_count;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }
}
