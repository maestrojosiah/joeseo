<?php

namespace App\Entity;

use App\Repository\KeywordRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: KeywordRepository::class)]
class Keyword
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'keywords')]
    private ?Project $project = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $keyword = null;

    #[ORM\Column(nullable: true)]
    private ?int $search_volume = null;

    #[ORM\Column(nullable: true)]
    private ?int $current_ranking = null;

    #[ORM\Column(nullable: true)]
    private ?int $target_ranking = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $keyword_type = null;

    #[ORM\Column(nullable: true)]
    private ?int $keyword_difficulty = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $creation_date = null;

    #[ORM\OneToMany(mappedBy: 'keyword', targetEntity: RankingHistory::class)]
    private Collection $rankingHistories;

    public function __toSTring()
    {
        return $this->keyword;
    }


    public function __construct()
    {
        $this->rankingHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;

        return $this;
    }

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): static
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getSearchVolume(): ?int
    {
        return $this->search_volume;
    }

    public function setSearchVolume(?int $search_volume): static
    {
        $this->search_volume = $search_volume;

        return $this;
    }

    public function getCurrentRanking(): ?int
    {
        return $this->current_ranking;
    }

    public function setCurrentRanking(?int $current_ranking): static
    {
        $this->current_ranking = $current_ranking;

        return $this;
    }

    public function getTargetRanking(): ?int
    {
        return $this->target_ranking;
    }

    public function setTargetRanking(?int $target_ranking): static
    {
        $this->target_ranking = $target_ranking;

        return $this;
    }

    public function getKeywordType(): ?string
    {
        return $this->keyword_type;
    }

    public function setKeywordType(?string $keyword_type): static
    {
        $this->keyword_type = $keyword_type;

        return $this;
    }

    public function getKeywordDifficulty(): ?int
    {
        return $this->keyword_difficulty;
    }

    public function setKeywordDifficulty(?int $keyword_difficulty): static
    {
        $this->keyword_difficulty = $keyword_difficulty;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeImmutable
    {
        return $this->creation_date;
    }

    public function setCreationDate(?\DateTimeImmutable $creation_date): static
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    /**
     * @return Collection<int, RankingHistory>
     */
    public function getRankingHistories(): Collection
    {
        return $this->rankingHistories;
    }

    public function addRankingHistory(RankingHistory $rankingHistory): static
    {
        if (!$this->rankingHistories->contains($rankingHistory)) {
            $this->rankingHistories->add($rankingHistory);
            $rankingHistory->setKeyword($this);
        }

        return $this;
    }

    public function removeRankingHistory(RankingHistory $rankingHistory): static
    {
        if ($this->rankingHistories->removeElement($rankingHistory)) {
            // set the owning side to null (unless already changed)
            if ($rankingHistory->getKeyword() === $this) {
                $rankingHistory->setKeyword(null);
            }
        }

        return $this;
    }
}
