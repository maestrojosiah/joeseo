<?php

namespace App\Entity;

use App\Repository\RankingHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RankingHistoryRepository::class)]
class RankingHistory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'rankingHistories')]
    private ?Keyword $keyword = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $ranking_date = null;

    #[ORM\Column(nullable: true)]
    private ?int $ranking_position = null;

    public function __toSTring()
    {
        return (string)$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyword(): ?Keyword
    {
        return $this->keyword;
    }

    public function setKeyword(?Keyword $keyword): static
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getRankingDate(): ?\DateTimeImmutable
    {
        return $this->ranking_date;
    }

    public function setRankingDate(?\DateTimeImmutable $ranking_date): static
    {
        $this->ranking_date = $ranking_date;

        return $this;
    }

    public function getRankingPosition(): ?int
    {
        return $this->ranking_position;
    }

    public function setRankingPosition(?int $ranking_position): static
    {
        $this->ranking_position = $ranking_position;

        return $this;
    }
}
