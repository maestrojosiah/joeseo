<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProjectRepository::class)]
class Project
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'projects')]
    private ?Client $client = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $project_name = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $start_date = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $end_date = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $project_status = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2, nullable: true)]
    private ?string $budget = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $priority_level = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $project_type = null;

    #[ORM\ManyToOne(inversedBy: 'projects')]
    private ?User $project_manager = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $creation_date = null;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Task::class)]
    private Collection $tasks;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Keyword::class)]
    private Collection $keywords;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Report::class)]
    private Collection $reports;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $project_image = null;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Content::class)]
    private Collection $contents;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Billing::class)]
    private Collection $billings;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: StrategyAdjustments::class)]
    private Collection $strategyAdjustments;

    #[ORM\OneToMany(mappedBy: 'project', targetEntity: Email::class)]
    private Collection $emails;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $project_description = null;

    public function __toSTring()
    {
        return $this->project_name;
    }

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->keywords = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->contents = new ArrayCollection();
        $this->billings = new ArrayCollection();
        $this->strategyAdjustments = new ArrayCollection();
        $this->emails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): static
    {
        $this->client = $client;

        return $this;
    }

    public function getProjectName(): ?string
    {
        return $this->project_name;
    }

    public function setProjectName(?string $project_name): static
    {
        $this->project_name = $project_name;

        return $this;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->start_date;
    }

    public function setStartDate(?\DateTimeImmutable $start_date): static
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->end_date;
    }

    public function setEndDate(?\DateTimeImmutable $end_date): static
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getProjectStatus(): ?string
    {
        return $this->project_status;
    }

    public function setProjectStatus(?string $project_status): static
    {
        $this->project_status = $project_status;

        return $this;
    }

    public function getBudget(): ?string
    {
        return $this->budget;
    }

    public function setBudget(?string $budget): static
    {
        $this->budget = $budget;

        return $this;
    }

    public function getPriorityLevel(): ?string
    {
        return $this->priority_level;
    }

    public function setPriorityLevel(?string $priority_level): static
    {
        $this->priority_level = $priority_level;

        return $this;
    }

    public function getProjectType(): ?string
    {
        return $this->project_type;
    }

    public function setProjectType(?string $project_type): static
    {
        $this->project_type = $project_type;

        return $this;
    }

    public function getProjectManager(): ?User
    {
        return $this->project_manager;
    }

    public function setProjectManager(?User $project_manager): static
    {
        $this->project_manager = $project_manager;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeImmutable
    {
        return $this->creation_date;
    }

    public function setCreationDate(?\DateTimeImmutable $creation_date): static
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    /**
     * @return Collection<int, Task>
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): static
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks->add($task);
            $task->setProject($this);
        }

        return $this;
    }

    public function removeTask(Task $task): static
    {
        if ($this->tasks->removeElement($task)) {
            // set the owning side to null (unless already changed)
            if ($task->getProject() === $this) {
                $task->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Keyword>
     */
    public function getKeywords(): Collection
    {
        return $this->keywords;
    }

    public function addKeyword(Keyword $keyword): static
    {
        if (!$this->keywords->contains($keyword)) {
            $this->keywords->add($keyword);
            $keyword->setProject($this);
        }

        return $this;
    }

    public function removeKeyword(Keyword $keyword): static
    {
        if ($this->keywords->removeElement($keyword)) {
            // set the owning side to null (unless already changed)
            if ($keyword->getProject() === $this) {
                $keyword->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Report>
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): static
    {
        if (!$this->reports->contains($report)) {
            $this->reports->add($report);
            $report->setProject($this);
        }

        return $this;
    }

    public function removeReport(Report $report): static
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getProject() === $this) {
                $report->setProject(null);
            }
        }

        return $this;
    }

    public function getProjectImage(): ?string
    {
        return $this->project_image;
    }

    public function setProjectImage(?string $project_image): static
    {
        $this->project_image = $project_image;

        return $this;
    }

    /**
     * @return Collection<int, Content>
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): static
    {
        if (!$this->contents->contains($content)) {
            $this->contents->add($content);
            $content->setProject($this);
        }

        return $this;
    }

    public function removeContent(Content $content): static
    {
        if ($this->contents->removeElement($content)) {
            // set the owning side to null (unless already changed)
            if ($content->getProject() === $this) {
                $content->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Billing>
     */
    public function getBillings(): Collection
    {
        return $this->billings;
    }

    public function addBilling(Billing $billing): static
    {
        if (!$this->billings->contains($billing)) {
            $this->billings->add($billing);
            $billing->setProject($this);
        }

        return $this;
    }

    public function removeBilling(Billing $billing): static
    {
        if ($this->billings->removeElement($billing)) {
            // set the owning side to null (unless already changed)
            if ($billing->getProject() === $this) {
                $billing->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, StrategyAdjustments>
     */
    public function getStrategyAdjustments(): Collection
    {
        return $this->strategyAdjustments;
    }

    public function addStrategyAdjustment(StrategyAdjustments $strategyAdjustment): static
    {
        if (!$this->strategyAdjustments->contains($strategyAdjustment)) {
            $this->strategyAdjustments->add($strategyAdjustment);
            $strategyAdjustment->setProject($this);
        }

        return $this;
    }

    public function removeStrategyAdjustment(StrategyAdjustments $strategyAdjustment): static
    {
        if ($this->strategyAdjustments->removeElement($strategyAdjustment)) {
            // set the owning side to null (unless already changed)
            if ($strategyAdjustment->getProject() === $this) {
                $strategyAdjustment->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Email>
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(Email $email): static
    {
        if (!$this->emails->contains($email)) {
            $this->emails->add($email);
            $email->setProject($this);
        }

        return $this;
    }

    public function removeEmail(Email $email): static
    {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getProject() === $this) {
                $email->setProject(null);
            }
        }

        return $this;
    }

    public function getProjectDescription(): ?string
    {
        return $this->project_description;
    }

    public function setProjectDescription(?string $project_description): static
    {
        $this->project_description = $project_description;

        return $this;
    }
}
