<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TaskRepository::class)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'tasks')]
    private ?Project $project = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $task_description = null;

    #[ORM\ManyToOne(inversedBy: 'tasks')]
    private ?User $assigned_to = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deadline = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $task_status = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $task_priority = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $creation_date = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $task_duration = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $start_date = null;

    #[ORM\Column(nullable: true)]
    private ?int $month = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $task_name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toSTring()
    {
        return $this->task_name;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;

        return $this;
    }

    public function getTaskDescription(): ?string
    {
        return $this->task_description;
    }

    public function setTaskDescription(?string $task_description): static
    {
        $this->task_description = $task_description;

        return $this;
    }

    public function getAssignedTo(): ?User
    {
        return $this->assigned_to;
    }

    public function setAssignedTo(?User $assigned_to): static
    {
        $this->assigned_to = $assigned_to;

        return $this;
    }

    public function getDeadline(): ?\DateTimeImmutable
    {
        return $this->deadline;
    }

    public function setDeadline(\DateTimeImmutable $deadline): static
    {
        $this->deadline = $deadline;

        return $this;
    }

    public function getTaskStatus(): ?string
    {
        return $this->task_status;
    }

    public function setTaskStatus(?string $task_status): static
    {
        $this->task_status = $task_status;

        return $this;
    }

    public function getTaskPriority(): ?string
    {
        return $this->task_priority;
    }

    public function setTaskPriority(?string $task_priority): static
    {
        $this->task_priority = $task_priority;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeImmutable
    {
        return $this->creation_date;
    }

    public function setCreationDate(\DateTimeImmutable $creation_date): static
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    public function getTaskDuration(): ?string
    {
        return $this->task_duration;
    }

    public function setTaskDuration(?string $task_duration): static
    {
        $this->task_duration = $task_duration;

        return $this;
    }

    public function getStartDate(): ?\DateTimeImmutable
    {
        return $this->start_date;
    }

    public function setStartDate(?\DateTimeImmutable $start_date): static
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getMonth(): ?int
    {
        return $this->month;
    }

    public function setMonth(?int $month): static
    {
        $this->month = $month;

        return $this;
    }

    public function getTaskName(): ?string
    {
        return $this->task_name;
    }

    public function setTaskName(?string $task_name): static
    {
        $this->task_name = $task_name;

        return $this;
    }
}
