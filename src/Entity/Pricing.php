<?php

namespace App\Entity;

use App\Repository\PricingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PricingRepository::class)]
class Pricing
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pricing_category = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 0, nullable: true)]
    private ?string $price = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $currency = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $billing_cycle = null;

    #[ORM\ManyToMany(targetEntity: Service::class, inversedBy: 'pricings')]
    private Collection $services;

    public function __toSTring()
    {
        return $this->getServiceName();
    }

    public function __construct()
    {
        $this->services = new ArrayCollection();
    }

    public function getServiceName(): string
    {
        return $this->pricing_category . "-" . $this->billing_cycle;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPricingCategory(): ?string
    {
        return $this->pricing_category;
    }

    public function setPricingCategory(?string $pricing_category): static
    {
        $this->pricing_category = $pricing_category;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): static
    {
        $this->currency = $currency;

        return $this;
    }

    public function getBillingCycle(): ?string
    {
        return $this->billing_cycle;
    }

    public function setBillingCycle(?string $billing_cycle): static
    {
        $this->billing_cycle = $billing_cycle;

        return $this;
    }

    /**
     * @return Collection<int, Service>
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): static
    {
        if (!$this->services->contains($service)) {
            $this->services->add($service);
        }

        return $this;
    }

    public function removeService(Service $service): static
    {
        $this->services->removeElement($service);

        return $this;
    }
}
