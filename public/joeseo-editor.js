document.addEventListener('DOMContentLoaded', () => {
  const textareas = document.querySelectorAll('textarea.joeseo-editor');

  textareas.forEach((textarea) => {
      const editorWrapper = document.createElement('div');
      editorWrapper.style.marginBottom = '10px';

      const toolbar = document.createElement('div');
      toolbar.style.border = '1px solid #555';
      toolbar.style.background = '#2c2c2c';
      toolbar.style.padding = '8px';
      toolbar.style.display = 'flex';
      toolbar.style.gap = '8px';
      toolbar.style.borderRadius = '5px';

      // Add button styles here
      toolbar.innerHTML = `
          <button onclick="format('bold')" style="font-weight:bold; padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">B</button>
          <button onclick="format('italic')" style="font-style:italic; padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">I</button>
          <button onclick="format('insertOrderedList')" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">OL</button>
          <button onclick="format('insertUnorderedList')" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">UL</button>
          <button onclick="formatHeading(1)" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">H1</button>
          <button onclick="formatHeading(2)" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">H2</button>
          <button onclick="formatHeading(3)" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">H3</button>
          <button onclick="formatHeading(4)" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">H4</button>
          <button onclick="formatHeading(5)" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">H5</button>
          <button onclick="formatParagraph()" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">P</button>
          <button onclick="createLink()" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">Link</button>
          <button onclick="insertImage()" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">Image</button>
          <button onclick="toggleSource()" style="padding:5px 10px; border-radius:4px; background:#444; color:#fff; border:none; cursor:pointer;">Source</button>
      `;

      editorWrapper.appendChild(toolbar);

      const editor = document.createElement('div');
      editor.contentEditable = true;
      editor.style.width = '100%';
      editor.style.minHeight = '200px';
      editor.style.border = '1px solid #555';
      editor.style.padding = '12px';
      editor.style.marginTop = '10px';
      editor.style.backgroundColor = '#333';
      editor.style.color = '#eee';
      editor.style.borderRadius = '5px';
      editor.style.fontFamily = 'Arial, sans-serif';
      editor.innerHTML = textarea.value;

      editorWrapper.appendChild(editor);

      const source = document.createElement('textarea');
      source.style.width = '100%';
      source.style.minHeight = '200px';
      source.style.display = 'none';
      source.style.marginTop = '10px';
      source.style.padding = '12px';
      source.style.border = '1px solid #555';
      source.style.backgroundColor = '#333';
      source.style.color = '#eee';
      source.style.borderRadius = '5px';
      editorWrapper.appendChild(source);

      textarea.style.display = 'none';
      textarea.parentNode.insertBefore(editorWrapper, textarea);

      window.format = (command, value = null) => {
          document.execCommand(command, false, value);
      };

      window.formatHeading = (level) => {
          format('formatBlock', 'h' + level);
      };

      window.formatParagraph = () => {
          format('formatBlock', 'p');
      };

      window.createLink = () => {
          const url = prompt("Enter the URL");
          if (url) {
              format('createLink', url);
          }
      };

      window.insertImage = () => {
          const url = prompt("Enter the image URL");
          if (url) {
              format('insertImage', url);
          }
      };

      window.toggleSource = () => {
          if (source.style.display === 'none') {
              source.value = editor.innerHTML;
              editor.style.display = 'none';
              source.style.display = 'block';
          } else {
              editor.innerHTML = source.value;
              editor.style.display = 'block';
              source.style.display = 'none';
          }
      };

      textarea.form.addEventListener('submit', () => {
          textarea.value = (source.style.display === 'none') ? editor.innerHTML : source.value;
      });
  });
});
